package assignment;
public class SavingAccount extends BankAccount{
	
	public SavingAccount(){
		this.rate = 7;
	}
	
	public int rateOfInterest(){
		return this.rate;
	}
}