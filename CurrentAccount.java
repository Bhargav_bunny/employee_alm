package assignment;
public class CurrentAccount extends BankAccount{
	
	public CurrentAccount(){
		this.rate = 4;
	}
	public int rateOfInterest(){
		return this.rate;
	}
}